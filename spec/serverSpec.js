const request = require("request");
require("../index");

const URI = process.env.TEST_URI;

// Data definition, used in test cases
const USER_ID = 0;
const FAKE_USER_ID = 10;
const ALBUM = {
	id: "abc",
	name: "album name",
	release_date: "01-01-2022",
	total_tracks: 10,
	image: "image url",
};

describe("Server", () => {
	describe("GET /users", () => {
		// Get all users available in the app
		let result = {};
		beforeAll((done) => {
			request.get(`${URI}/users`, function (error, response, body) {
				result.status = response.statusCode;
				result.body = JSON.parse(body);
				done();
			});
		});
		it("Should send status 200", (done) => {
			expect(result.status).toBe(200);
			done();
		});
		it("Should have 2 users", (done) => {
			expect(result.body.length).toBe(2);
			done();
		});
	});
	describe("GET /users/:userId/albums", () => {
		// Get all albums from a specific user's library
		let result = {};
		beforeAll((done) => {
			request.get(
				`${URI}/users/${USER_ID}/albums`,
				function (error, response, body) {
					result.status = response.statusCode;
					result.body = JSON.parse(body);
					done();
				}
			);
		});
		it("Should send status 200", (done) => {
			expect(result.status).toBe(200);
			done();
		});
		it("Should send status 400 for unexisting user", (done) => {
			request.get(
				`${URI}/users/${FAKE_USER_ID}/albums`,
				function (error, response) {
					expect(response.statusCode).toBe(400);
					done();
				}
			);
		});
		it("Should have 0 album", (done) => {
			expect(result.body.length).toBe(0);
			done();
		});
	});
	describe("POST /users/:userId/albums", () => {
		// Add an album to a user's library
		it("Should send status 400 with non appropriate data", (done) => {
			request.post(
				`${URI}/users/${USER_ID}/albums`,
				{ json: true, body: {} },
				function (error, response) {
					expect(response.statusCode).toBe(400);
					done();
				}
			);
		});
		it("Should send status 200 and return album informations", (done) => {
			request.post(
				`${URI}/users/${USER_ID}/albums`,
				{ json: true, body: ALBUM },
				function (error, response, body) {
					expect(response.statusCode).toBe(200);
					expect(body.id).toEqual(ALBUM.id);
					done();
				}
			);
		});
		it("Should not add duplicate album", (done) => {
			request.post(
				`${URI}/users/${USER_ID}/albums`,
				{ json: true, body: ALBUM },
				function (error, response) {
					expect(response.statusCode).toBe(400);
					done();
				}
			);
		});
	});
	describe("PATCH /users/:userId/albums/:id", () => {
		// Set an album as a favorite in a user's library
		it("Should send status 400 with non appropriate data", (done) => {
			request.patch(
				`${URI}/users/${USER_ID}/albums/${ALBUM.id}`,
				{ json: true, body: { favorite: "test" } },
				function (error, response) {
					expect(response.statusCode).toBe(400);
					done();
				}
			);
		});
		it("Should send status 200 and set album as favorite", (done) => {
			request.patch(
				`${URI}/users/${USER_ID}/albums/${ALBUM.id}`,
				{ json: true, body: { favorite: true } },
				function (error, response, body) {
					expect(response.statusCode).toBe(200);
					expect(body.favorite).toBe(true);
					done();
				}
			);
		});
	});
	describe("DELETE /users/:userId/albums/:id", () => {
		// Remove an album from a library
		it("Should have 1 album", (done) => {
			request.get(
				`${URI}/users/${USER_ID}/albums`,
				function (error, response, body) {
					expect(response.statusCode).toBe(200);
					const result = JSON.parse(body);
					expect(result.length).toBe(1);
					done();
				}
			);
		});
		it("Should send status 200 on delete album", (done) => {
			request.delete(
				`${URI}/users/${USER_ID}/albums/${ALBUM.id}`,
				function (error, response) {
					expect(response.statusCode).toBe(200);
					done();
				}
			);
		});
		it("Should have 0 album", (done) => {
			request.get(
				`${URI}/users/${USER_ID}/albums`,
				function (error, response, body) {
					expect(response.statusCode).toBe(200);
					const result = JSON.parse(body);
					expect(result.length).toBe(0);
					done();
				}
			);
		});
	});
});
