const database = require("./default_database.json"); // Could have used a real database, or saved modifications in a json file to store data even if the server restarts
const express = require("express");
const axios = require("axios");
const router = express.Router();

const token = process.env.API_TOKEN;
const apiUrl = "https://api.spotify.com/";

router.get("/search", (req, res) => {
	if (!req.query.q) return res.sendStatus(400);

	// Request to the Spotify API to search for the albums
	axios
		.get(`${apiUrl}v1/search?type=album&q=${req.query.q}`, {
			headers: { Authorization: `Bearer ${token}` },
		})
		.then((r) => {
			if (
				!r.data ||
				!r.data.albums ||
				!Array.isArray(r.data.albums.items)
			)
				return res.sendStatus(500);
			const results = r.data.albums.items.map((item) => {
				return {
					id: item.id,
					name: item.name,
					release_date: item.release_date,
					total_tracks: item.total_tracks,
					image: item.images[0].url,
				};
			});
			res.send(results);
		})
		.catch((e) => {
			console.error(e);
			res.sendStatus(500);
		});
});

router.get("/users", (req, res) => {
	// Get all users available in the app
	res.send(
		database.map((user, index) => ({
			id: index,
			username: user.username,
		}))
	);
});

router.get("/users/:userId/albums", (req, res) => {
	// Get all albums from a specific user's library
	if (!database[req.params.userId])
		return res.status(400).send({ message: "User not found" });
	res.send(database[req.params.userId].albums);
});

router.post("/users/:userId/albums", (req, res) => {
	// Add an album to a user's library
	if (!database[req.params.userId])
		return res.status(400).send({ message: "User not found" });
	if (
		!req.body.id ||
		!req.body.name ||
		!req.body.release_date ||
		!req.body.total_tracks ||
		!req.body.image
	)
		return res.sendStatus(400);
	if (database[req.params.userId].albums.some((a) => a.id == req.body.id))
		return res.sendStatus(400); // Cannot add twice the same album
	const album = {
		id: req.body.id,
		name: req.body.name,
		release_date: req.body.release_date,
		total_tracks: req.body.total_tracks,
		image: req.body.image,
		favorite: false,
		tags: [],
	};
	database[req.params.userId].albums.push(album);
	res.send(album);
});

router.delete("/users/:userId/albums/:id", (req, res) => {
	// Remove an album from a library
	if (!database[req.params.userId])
		return res.status(400).send({ message: "User not found" });
	database[req.params.userId].albums = database[
		req.params.userId
	].albums.filter((a) => a.id != req.params.id);
	res.sendStatus(200);
});

router.patch("/users/:userId/albums/:id", (req, res) => {
	// Set the favorite property of an album
	if (!database[req.params.userId])
		return res.status(400).send({ message: "User not found" });
	if (typeof req.body.favorite != "boolean") return res.sendStatus(400);
	const index = database[req.params.userId].albums.findIndex(
		(a) => a.id == req.params.id
	);
	if (index == -1) return res.sendStatus(400);
	database[req.params.userId].albums[index].favorite = req.body.favorite;
	res.send(database[req.params.userId].albums[index]);
});

router.patch("/users/:userId/albums", (req, res) => {
	// Add a tag for one or multiple album(s)
	if (!database[req.params.userId])
		return res.status(400).send({ message: "User not found" });
	if (!req.body.tag || !Array.isArray(req.body.ids))
		return res.sendStatus(400);
	database[req.params.userId].albums = database[req.params.userId].albums.map(
		(a) => {
			if (!req.body.ids.includes(a.id)) return a;
			if (a.tags.includes(req.body.tag)) return a;
			a.tags.push(req.body.tag);
			return a;
		}
	);
	res.send(database[req.params.userId].albums);
});

module.exports = router;
