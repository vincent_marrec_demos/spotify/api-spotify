# spotify

## Project setup
Make sure to configure environment variables in a .env file like shown in the .env.example file

To obtain a Spotify API Token, create or login to a spotify account on the [Spotify for Developers Website](https://developer.spotify.com/dashboard/login).
Access to the Dashboard page and create an application. You should now have access to a Client ID and a Client Secret.
You can use these informations to get an API Token by following this [documentation](https://developer.spotify.com/documentation/general/guides/authorization/client-credentials/). You can then use your token in the .env file. The token is available only for 1 hour, but you can recreate one later.

```
npm install
```

### Hot-reload for development
```
npm run dev
```

### Run server
```
npm run start
```

### Run tests
```
npm run test
```
